core = 7.x
api  = 2

libraries[chardin][download][type]    = git
libraries[chardin][download][url]     = https://github.com/heelhook/chardin.js.git
