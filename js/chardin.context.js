(function ($) {
  Drupal.behaviors.chardinContext = {
    attach:function (context, settings) {
			// don't run rest of script unless there is a context
      if(!Drupal.settings.chardinContext) return false;
		
			// grab all context elements.
			var elements = Drupal.settings.chardinContext.elements;
			
			$.each(elements, function (key, chardin) {
				// add chardin attributes to elements set in the context
				if(chardin.position != 'none'){
					$(chardin.element).attr({
						'data-intro': chardin.intro,
						'data-position': chardin.position,
					});
				}
				// adds required attribute to make element a toggler
				if(chardin.toggle == true){
					$(chardin.element).attr({
						'data-toggle': 'chardinjs',
					});
				}
			});
			
			// activates any toggle elements
			$('a[data-toggle="chardinjs"]').on('click', function(e) {
				e.preventDefault();
				return ($('body').data('chardinJs')).toggle();
			});
			
			// set auto_start and check of auto start is on.
			var auto_start = Drupal.settings.chardinContext.auto_start;
			if(auto_start == true) {
				$('body').chardinJs('start');
			} else {
				$('body').chardinJs();
			}			
    }
  };
}(jQuery));